#include <algorithm> 
#include <iostream>
#include "Area.h"
#include "Player.h"
#include <Windows.h>

using std::find;
using std::cout;
using std::cin;
using std::endl;


Area::Area(string name)
{
	_areaName = name;
}

string Area::getName()
{
	return _areaName;
}

bool Area::addRoom(string key, Room* room)
{
	if (_area.find(key) != _area.end())
	{
		return false;
	}

	_area.insert(std::pair<string, Room*>(key, room));
	return true;
}

vector<BaseItem*> Area::setRoom(Room* room)
{
	_currentRoom = room;
	vector<BaseItem*> temp;

	if (room->getCombatEvent().size() > 0)
	{
		for each (Event* var in room->getCombatEvent())
		{
			if (var->getActive())
				temp = var->runEvent();
		}
	}
	return temp;
}

Room* Area::getRoom()
{
	return _currentRoom;
}

void Area::showArea(Area* area)
{
	cout << endl << area->getName() << " - " << area->_currentRoom->getName() << endl << endl;

	cout << area->_currentRoom->getDesc() << endl;
}

void Area::linkExits(string key1, string key2)
{
	Room* temp1 = _area.find(key1)->second;
	Room* temp2 = _area.find(key2)->second;

	_area[key1]->setRoomExit(temp1->_exits.size() + 1, _area[key2]);
	_area[key2]->setRoomExit(temp2->_exits.size() + 1, _area[key1]);
}

void Area::removeAreaExit(int index)
{
	_exits.erase(_exits.begin() + sizeof(Area));
}

void Area::listExits()
{
	for (size_t i = 0; i < _exits.size(); i++)
	{
		cout << "[" << i + 1 << "] " << getAreaExit(i)->getName() << endl;
	}
	cout << "[0] Cancel" << endl;
}

Room* Area::operator[](string key)
{
	return _area[key];
}

Area* Area::getAreaExit(int index) const
{
	return _exits[index];
}

bool Area::setAreaExit(int index, Area* area)
{
	if (area == nullptr)
	{
		return false;
	}

	_exits.push_back(area);
	return true;
}

vector<Area*> Area::getExits()
{
	return _exits;
}

void Area::move()
{
	int choice = 0;

	do
	{
		cout << endl << "Which way would you like to go?" << endl;
		_currentRoom->listExits();
		cout << "> ";

		if (!(cin >> choice) || choice <0 || choice > _currentRoom->getExitSize())
		{
			cin.clear();
			cin.ignore(UINT_MAX, '\n');
			cout << "Invalid choice." << endl;
			Sleep(1000);
			system("cls");
			showArea(this);
			cout << endl << endl;
		}
		else if (choice == 0)
		{
			return;
		}
		else
		{
			break;
		}

	} while (true);


	if (move(choice) == false)
	{
		cout << endl << "You can't go that way." << endl;
	}
}

bool Area::move(int index)
{
	if (_currentRoom != nullptr)
	{
		if (_currentRoom->getExitSize() >= index && index > 0)
		{
			setRoom(_currentRoom->getRoomExit(index - 1));
			return true;
		}
	}
	return false;
}

