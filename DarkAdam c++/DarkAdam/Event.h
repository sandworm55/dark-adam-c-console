#include <vector>
#include "BaseItem.h"

#ifndef EVENT_H
#define EVENT_H

using std::vector;

class Event
{
private:
	string _name;
	vector<BaseItem*> _rewards;

	bool _active;
	Event* _evnt;
	int _type;

public:
	enum STATS { COMBAT, CONVO, INTER, ACTIVE, CHAR_ADD };
	Event(string name, vector<BaseItem*> rewards, bool active, int type, Event* evnt = nullptr) :
		_name(name), _rewards(rewards), _active(active), _evnt(evnt), _type(type) {}

	string getName() { return _name; }
	vector<BaseItem*> getRewards() { return _rewards; }
	bool getActive() { return _active; }
	int getType() const { return _type; }

	void runCallback() { if (_evnt != nullptr) _evnt->runEvent(); }

	void setActive(bool active) { _active = active; }

	//Does not return all chained event rewards
	virtual vector<BaseItem*> runEvent() = 0;

};
#endif