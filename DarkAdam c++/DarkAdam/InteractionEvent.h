#include "Event.h"
#include <string>
#include <iostream>

#ifndef INTERACTION_EVENT_H
#define INTERACTION_EVENT_H

using std::string;
using std::cout;
using std::endl;

class InteractionEvent : public Event
{
private:
	string _desc;
public:
	InteractionEvent(string name, vector<BaseItem*> items, string desc, bool active = true, Event* evnt = nullptr) :
		Event(name, items, active,Event::INTER ,evnt), _desc(desc)
	{}

	vector<BaseItem*> runEvent();
};

#endif