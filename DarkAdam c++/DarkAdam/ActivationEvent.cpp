#include "ActivationEvent.h"
vector<BaseItem*> ActivationEvent::runEvent()
{
	bool triggered = true;
	for each (Event* var in _flags)
	{
		if (var->getActive())
			triggered = false;

		if (triggered)
		{
			if (_eventToToggle == nullptr)
			{
				_firstArea->setAreaExit(_firstArea->getExits().size() + 1, _secondArea);
				_secondArea->setAreaExit(_secondArea->getExits().size() + 1, _firstArea);
			}
			else
				_eventToToggle->setActive(true);
		}
	}
	return getRewards();
}