#ifndef ARMOR_H
#define ARMOR_H

#include "Equipment.h"

class Armor : public Equipment
{
public:
	Armor(string name, string description,int itemType, int slot, int effect);

private:
	int checkSlot(int slot);
};

#endif