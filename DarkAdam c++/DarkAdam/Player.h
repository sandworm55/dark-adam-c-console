#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"
#include <vector>

using std::vector;

class Player
{
public:
	Player();
	~Player();

	void addCharacter(Character* character);
	Character* getCharacter(int index);
	vector<Character*> getParty();
	vector<Character*>* getPartyPointer();

	bool partyDead();

	void pickUpItem(BaseItem* item);

	bool hasItems();
	void sortInventory();
	vector<int> displayNums(int itemType);
	vector<int> displayNums(int itemType, int slot);
	void displayInventory(int itemType, int first, int count);
	BaseItem* getInventoryItem(int index) {	return _inventory[index-1];}
	void removeItem(int index) { _inventory.erase(_inventory.begin() + index-1); }
	void displayCharacters();
	void displayCharacters(int slot);

private:
	vector<BaseItem*> _inventory;
	vector<Character*> _party;
};


#endif
