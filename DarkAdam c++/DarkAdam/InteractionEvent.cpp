#include "InteractionEvent.h"

vector<BaseItem*> InteractionEvent::runEvent()
{
	setActive(false);
	cout << _desc << endl;
	runCallback();
	return getRewards();
}
