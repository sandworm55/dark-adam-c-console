#ifndef EQUIPMENT_H
#define EQUIPMENT_H

#include <iostream>
#include <vector>
#include "BaseItem.h"

using std::vector;

class Equipment : public BaseItem
{
public:
	Equipment(string name, string description,int itemType, int slot, int attack,int defence,int speed,int health);

	enum SLOTS { NOSLOT = -1, HEAD, CHEST, HANDS, LEGS, FEET, WEAPON, COUNT };
	enum STATS { ATTACK, DEFENCE, SPEED, HEALTH, STATS_COUNT };
};


#endif