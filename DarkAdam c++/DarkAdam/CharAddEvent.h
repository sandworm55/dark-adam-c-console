#ifndef CHAR_ADD_EVENT_H
#define CHAR_ADD_EVENT_H

#include "Event.h"
#include "Character.h"
#include "Player.h"

class CharAddEvent : public Event
{
public:
	CharAddEvent(string name, vector<BaseItem*> items, Character* characterToAdd, Player* player, Event* evnt = nullptr) :
		Event(name, items, true, Event::CHAR_ADD, evnt), _player(player), _characterToAdd(characterToAdd)
	{}

	vector<BaseItem*> runEvent()
	{
		_player->addCharacter(_characterToAdd);
		runCallback();
		return getRewards();
	}

private:
	Player* _player;
	Character* _characterToAdd;
};

#endif