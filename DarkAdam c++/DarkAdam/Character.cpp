#include "Character.h"
#include <stdlib.h>

using std::rand;

Character::Character(string name, int level, int maxHealth, int attack, int defence, int speed, bool isAlive, bool isPlayerChar) :
LifeForm(name, level, maxHealth, attack, defence, speed, isAlive, isPlayerChar)
{
	for (int i = 0; i < Equipment::COUNT; i++)
	{
		_equipment[i] = nullptr;
	}
}

Character::~Character()
{
	for (int i = 0; i < Equipment::COUNT; i++)
	{
		if (_equipment[i])
		{
			delete _equipment[i];
		}
	}
}

Equipment* Character::equipItem(Equipment* item)
{
	int slot = item->getSlot();
	Equipment* temp = nullptr;

	if (slot > Equipment::NOSLOT && slot < Equipment::COUNT)
	{
		if (_equipment[slot] != nullptr)
		{
			temp = _equipment[slot];
		}
		else
		{
			temp = nullptr;
		}

		_equipment[slot] = item;
	}

	return temp;
}

Equipment* Character::removeItem(int index)
{
	if (index > Equipment::NOSLOT &&  index < Equipment::COUNT)
	{
		Equipment* temp = _equipment[index];

		_equipment[index] = nullptr;

		return temp;
	}
	return nullptr;
}

string Character::getItemName(int slot) const
{
	string name;
	if (_equipment[slot] != nullptr)
		name = _equipment[slot]->getName();
	else
		name = "None";

	return name;
}

void Character::displayEquipment()
{
	cout << _name << " -> Equipment" << endl;
	cout << "[1]Head:\t " << (_equipment[Equipment::HEAD] != nullptr ? _equipment[Equipment::HEAD]->getName() : "---") << endl;
	cout << "[2]Chest:\t " << (_equipment[Equipment::CHEST] != nullptr ? _equipment[Equipment::CHEST]->getName() : "---") << endl;
	cout << "[3]Hands:\t " << (_equipment[Equipment::HANDS] != nullptr ? _equipment[Equipment::HANDS]->getName() : "---") << endl;
	cout << "[4]Legs:\t " << (_equipment[Equipment::LEGS] != nullptr ? _equipment[Equipment::LEGS]->getName() : "---") << endl;
	cout << "[5]Feet:\t " << (_equipment[Equipment::FEET] != nullptr ? _equipment[Equipment::FEET]->getName() : "---") << endl;
	cout << "[6]Weapon:\t " << (_equipment[Equipment::WEAPON] != nullptr ? _equipment[Equipment::WEAPON]->getName() : "---") << endl;
}

bool Character::hasEquipment()
{
	for (int i = 0; i < Equipment::COUNT; i++)
	{
		if (_equipment[i] != nullptr)
		{
			return true;
		}
	}
	return false;
}

void Character::displayStats()
{
	updateStats();

	cout << endl << "Character Stats" << endl
		<< "Name: " << _name << " Lvl: " << _level << endl
		<< "Health: " << getStat(Equipment::STATS::HEALTH) << "/" << _maxHealth + _healthBonus << endl;

	cout << "Defence: " << getStat(Equipment::STATS::DEFENCE) << endl;
	cout << "Attack: " << getStat(Equipment::STATS::ATTACK) << endl;
	cout << "Speed: " << getStat(Equipment::STATS::SPEED) << endl;
}

void Character::updateStats()
{
	_healthBonus = 0;
	_attackBonus = 0;
	_defenceBonus = 0;
	_speedBonus = 0;

	for (int i = 0; i < Equipment::COUNT; i++)
	{
		if (_equipment[i] != nullptr)
		{
			_attackBonus += (_equipment[i]->getStat(Equipment::STATS::ATTACK));
			_defenceBonus += (_equipment[i]->getStat(Equipment::STATS::DEFENCE));
			_speedBonus += (_equipment[i]->getStat(Equipment::STATS::SPEED));
			_healthBonus += (_equipment[i]->getStat(Equipment::STATS::HEALTH));
		}
	}
}

void Character::useConsumable(Character* character, BaseItem* item)
{

	if (character->isAlive() == item->getTargetAlive())
	{
		character->adjustCurrentHealth(item->getConsumableEffect());
		item->removeStack();
	}
	else
	{
		cout << "Target must be " << (item->getTargetAlive() ? "Alive" : "Dead") << "." << endl;
	}
}

void Character::attack(LifeForm* LifeForm)
{
	int dmg = LifeForm::getAttack() - LifeForm->getStat(Equipment::STATS::DEFENCE);

	if (dmg < 0) dmg = 0;

	cout << LifeForm->getName() << " took " << LifeForm->receiveDamage(dmg) << " damage from " << _name << endl;

}

int Character::getStat(int stat)
{
	int statReturn = 0;

	statReturn += LifeForm::getStat(stat);

	switch (stat)
	{
	case Equipment::STATS::ATTACK:
		statReturn += _attackBonus;
		break;
	case Equipment::STATS::DEFENCE:
		statReturn += _defenceBonus;
		break;
	case Equipment::STATS::HEALTH:
		statReturn += _healthBonus;
		break;
	case Equipment::STATS::SPEED:
		statReturn += _speedBonus;
		break;
	default:
		break;
	}
	return statReturn;
}