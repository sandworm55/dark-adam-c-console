#ifndef __AREA_H
#define __AREA_H

#include <string>
#include <vector>
#include <map>
#include <Windows.h>
#include "room.h"

using std::string;
using std::vector;
using std::map;

class Area
{
public:
	friend class WorldZone;

	Area(string name = "An unknown location.");
	~Area() {}

	//output info
	string getName();

	//rooms
	bool addRoom(string name, Room* room);
	vector<BaseItem*> setRoom(Room* room);
	Room* getRoom();
	void linkExits(string key, string key2);

	//read room pointer by map key
	Room* operator[](string key);

	//areas
	void showArea(Area* area);
	void listExits();
	Area* getAreaExit(int index) const;
	bool setAreaExit(int index, Area* area);
	void removeAreaExit(int index);
	vector<Area*> getExits();

	//move
	void move();
	bool move(int index);

private:
	string _areaName;
	Room* _currentRoom;
	map<string, Room*> _area;

	vector<Area*> _exits;
	vector<Room*> _rooms;
	//vector<BaseItem*> _items;
};

#endif
