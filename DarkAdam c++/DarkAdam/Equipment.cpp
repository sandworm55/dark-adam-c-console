#include "Equipment.h"

Equipment::Equipment(string name, string description, int itemType, int slot
					 , int attack, int defence, int speed, int health) : 
BaseItem(name,description,itemType)
{
	if (slot < HEAD || slot >= COUNT)
	{
		_slot = NOSLOT;
	}
	else
	{
		_slot = slot;
	}

	_stats[STATS::ATTACK] = attack;
	_stats[STATS::DEFENCE] = defence;
	_stats[STATS::SPEED] = speed;
	_stats[STATS::HEALTH] = health;
}