#include "Player.h"
#include <algorithm>   
#include <vector>

Player::Player()
{

};

Player::~Player()
{};

void Player::addCharacter(Character* character)
{
	_party.push_back(character);
}

Character* Player::getCharacter(int index)
{
	return _party[index - 1];
}

vector<Character*> Player::getParty()
{
	return _party;
}

vector<Character*>* Player::getPartyPointer()
{
	return &_party;
}

bool Player::partyDead()
{
	bool partyDead = true;
	for each (LifeForm* check in _party)
	{
		if (check->isAlive())
			partyDead = false;
	}

	return partyDead;
}

void Player::pickUpItem(BaseItem* item)
{
	//cout << item->getName()<<endl;
	if (_inventory.size() > 0)
	{
		if (item->getStackSize() > 0)
		{
			vector<string> temp;

			vector<int> consumableList = displayNums(item->getItemType());

			if (consumableList[1] > 0)
			{
				for (int i = consumableList[0]; i < consumableList[0] + consumableList[1]; i++)
				{
					temp.push_back(_inventory[i]->getName());
				}
				vector<string>::iterator index = find(temp.begin(), temp.end(), item->getName());

				if (index != temp.end())
				{
					int pos = std::distance(temp.begin(), index);
					pos += consumableList[0];

					while (item->getStackSize() > 0)
					{
						item->removeStack();
						_inventory[pos]->addStack();
					}

					if (item->getStackSize() == 0)
						delete item;

					return;
				}
			}
		}
	}

	_inventory.push_back(item);

}

bool Player::hasItems()
{
	return _inventory.size() > 0;
}

void Player::sortInventory()
{
	for (size_t i = 0; i < _inventory.size(); i++)
	{
		if (_inventory[i]->getItemType() < BaseItem::ITEM_TYPE::BASE || _inventory[i]->getItemType() >= BaseItem::ITEM_TYPE::COUNT)
		{
			_inventory.erase(_inventory.begin() + i);
		}
		for (size_t j = i + 1; j < _inventory.size(); j++)
		{
			if (_inventory[i]->getItemType() > _inventory[j]->getItemType())
			{
				BaseItem* temp = _inventory[j];
				_inventory[j] = _inventory[i];
				_inventory[i] = temp;
			}
			if (_inventory[i]->getItemType() == _inventory[j]->getItemType() &&
				_inventory[i]->getItemType() == BaseItem::ITEM_TYPE::ARMOR)
			{
				if (_inventory[i]->getSlot() > _inventory[j]->getSlot())
				{
					BaseItem* temp = _inventory[j];
					_inventory[j] = _inventory[i];
					_inventory[i] = temp;
				}
			}
		}
	}

}

vector<int> Player::displayNums(int itemType)
{
	sortInventory();

	vector<int> intReturn;

	if (itemType >= BaseItem::ITEM_TYPE::BASE && itemType < BaseItem::ITEM_TYPE::COUNT)
	{
 		int loop = 0;
		int count = 0;
		int first = 0;
		int type = 0;

		do
		{
			if (_inventory.size() != 0)
				type = _inventory[loop]->getItemType();

			if (type == itemType)
			{
				count += 1;
			}

			if (type > itemType)
			{
				break;
			}
			loop += 1;
		} while (loop < _inventory.size());

		first = loop - count;


		intReturn.push_back(first);
		intReturn.push_back(count);
		return intReturn;
	}

	if (itemType == BaseItem::ITEM_TYPE::COUNT)
	{
		intReturn.clear();
		intReturn.push_back(0);
		intReturn.push_back(_inventory.size());
		return intReturn;
	}

	return intReturn;
}

vector<int> Player::displayNums(int itemType, int slot)
{
	sortInventory();

	vector<BaseItem*> temp;
	vector<int> itemRange = displayNums(itemType);

	for (int i = itemRange[0]; i < itemRange[0] + itemRange[1]; i++)
	{
		temp.push_back(_inventory[i]);
	}

	vector<int> intReturn;

	if (slot > Equipment::SLOTS::NOSLOT && slot < Equipment::SLOTS::COUNT && temp.size() > 0)
	{
		int loop = 0;
		int count = 0;
		int first = 0;
		int type = 0;

		do
		{
			if (_inventory.size() != 0)
				type = temp[loop]->getSlot();

			if (type == slot)
			{
				count += 1;
			}

			if (type > slot)
			{
				break;
			}
			loop += 1;
		} while (loop < (int)temp.size());

		first = loop - count;
		first += itemRange[0];

		//vector<int> intReturn;
		intReturn.push_back(first);
		intReturn.push_back(count);
		return intReturn;
	}
	return intReturn;
}

void Player::displayInventory(int itemType, int first, int count)
{
	switch (itemType)
	{
	case BaseItem::ITEM_TYPE::BASE:
		cout << "BASIC: " << endl;
		break;
	case BaseItem::ITEM_TYPE::CONSUMABLES:
		cout << "CONSUMABLES: " << endl;
		break;
	case BaseItem::ITEM_TYPE::WEAPON:
		cout << "WEAPON: " << endl;
		break;
	case BaseItem::ITEM_TYPE::ARMOR:
		cout << "ARMOR: " << endl;
		break;
	case BaseItem::ITEM_TYPE::COUNT:
		cout << "ALL: " << endl;
		break;
	default:
		return;
	}

	for (int i = first; i < first + count; i++)
	{
		int n = 1 + i - first;
		cout << "\t[" << n << "]" << " " + _inventory[i]->getName() <<
			((_inventory[i]->getStackSize() > 0) ? " (" + std::to_string(_inventory[i]->getStackSize()) + ")" : " ") <<
			endl;
	}

	if (count == 0)
	{
		cout << "\tNO ITEMS" << endl;
		cout << "\t[0]Return" << endl;
	}
	else
	{
		cout << "\t[0] Cancel" << endl;
	}

}

void Player::displayCharacters()
{
	for (int i = 0; i < (int)_party.size(); i++)
	{
		cout << "[" << i + 1 << "] " << _party[i]->getName() << endl;
	}
	cout << "[0] Cancel" << endl;
}

void Player::displayCharacters(int slot)
{
	for (int i = 0; i < (int)_party.size(); i++)
	{
		cout << "[" << i + 1 << "] " << _party[i]->getName() << " (" << _party[i]->getItemName(slot) << ")" << endl;
	}
	cout << "[0] Cancel" << endl;
}