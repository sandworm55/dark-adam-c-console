#include <algorithm>
#include <iostream>
#include "Area.h"
#include "Event.h"

using std::find;
using std::cout;
using std::endl;

void Room::listExits()
{
	for (size_t i = 0; i < _exits.size(); i++)
	{
		cout << "[" << i + 1 << "] " << getRoomExit(i)->getName() << endl;
	}
	cout << "[0] Cancel" << endl;
}

bool Room::setRoomExit(int index, Room* room)
{
	if (room == nullptr)
	{
		return false;
	}

	_exits.push_back(room);
	return true;
}

void Room::removeRoomExit(int index)
{
	_exits.erase(_exits.begin() + sizeof(Room));
}

void Room::addItem(BaseItem* item)
{
	if (find(_items.begin(), _items.end(), item) != _items.end())
	{
		return;
	}

	_items.push_back(item);
}

BaseItem* Room::removeItem(size_t index)
{
	if (index < 1 || index > _items.size())
	{
		return nullptr;
	}

	int zeroBased = index - 1;
	BaseItem* item = _items.at(zeroBased);
	_items.erase(_items.begin() + zeroBased);

	return item;
}

void Room::listItems()
{
	for (size_t i = 0; i < _items.size(); i++)
	{
		cout << "[" << i + 1 << "] " << _items[i]->getName() << endl;
	}
}

bool Room::hasItems()
{
	if (_items.size() > 0)
	{
		return true;
	}
	return false;
}

bool Room::hasEvent()
{
	if (_events.size() > 0)
	{
		return true;
	}
	return false;
}

void Room::listEvents()
{
	for (size_t i = 0; i < _events.size(); i++)
	{
		cout << "[" << i + 1 << "] " << _events[i]->getName() << endl;
	}
	cout << "[0] Cancel" << endl;
}

vector<BaseItem*> Room::runEvent(size_t index)
{
	vector<BaseItem*> temp;
	if (index < 1 || index > _events.size())
	{
		temp.push_back(nullptr);
		return temp;
	}

	int zeroBased = index - 1;
	temp = _events.at(zeroBased)->runEvent();
	_events.erase(_events.begin() + zeroBased);

	if (_activeEvents.size() > 0)
	{
		for each (Event* var in _activeEvents)
		{
			if (var->getType() == Event::ACTIVE)
				var->runEvent();
		}
	}

	return temp;
}