#ifndef WORLD_ZONE_H
#define WORLD_ZONE_H

#include "Area.h"
#include "Player.h"
#include "BaseItem.h"
#include "Equipment.h"
#include "Consumable.h"
#include "LifeForm.h"
#include "Character.h"
#include "InteractionEvent.h"
#include "CombatEvent.h"

#include <string>
#include <map>
#include <iostream>

using std::string;
using std::map;
using std::cout;
using std::cin;
using std::endl;

class WorldZone
{
public:
	WorldZone();
	~WorldZone();

	//areas
	bool addArea(string key, Area* area);
	void setArea(Area* area);
	Area* getArea(string name) { return _world.find(name)->second; }
	void linkArea(string key1, string key2);

	//read area pointer by map key
	Area* operator[](string key);

	//
	void init();
	bool running();
	char queryAction();

	//player actions
	void move();
	bool move(int index);
	void take();
	void inventory();
	void use(Consumable* temp);
	bool equip(Equipment* temp);
	void unequip(Character* character);
	void stats();
	void events();

private:
	Area* _currentArea;
	map<string, Area*> _world;

	Player* _player;
	
};


#endif