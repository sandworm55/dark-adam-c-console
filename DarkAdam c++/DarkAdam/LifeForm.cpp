#include "LifeForm.h"

void LifeForm::attack(LifeForm* LifeForm)
{
	int dmg = LifeForm::getAttack() - LifeForm->getStat(Equipment::STATS::DEFENCE);

	if (dmg < 0)
	{
		dmg = 0;
		cout << LifeForm->getName() << " took " << LifeForm->receiveDamage(dmg) << " damage, " << _name << " missed the attack." << endl;
	}
	else
		cout << LifeForm->getName() << " took " << LifeForm->receiveDamage(dmg) << " damage from " << _name << endl;

}

int LifeForm::receiveDamage(unsigned int damage)
{
	adjustCurrentHealth(0 - damage);
	return damage;
}

int LifeForm::getStat(int stat)
{
	int statReturn = 0;

	switch (stat)
	{
		case (Equipment::STATS::ATTACK) :
			statReturn += _attack;
			break;
		case (Equipment::STATS::DEFENCE) :
			statReturn += _defence;
			break;
		case (Equipment::STATS::HEALTH) :
			statReturn += _currentHealth;
			break;
		case (Equipment::STATS::SPEED) :
			statReturn += _speed;
			break;
		default:
			return 0;
	}
	return statReturn;
}

void LifeForm::displayStats()
{
	cout << endl << "Character Sheet" << endl;
	cout << "Type: " << _name << endl;
	cout << "Level: " << _level << "\tHealth: " << _currentHealth << "/" << _maxHealth << endl;
}

int LifeForm::getAttack()
{
	int tempAttack = getStat(Equipment::STATS::ATTACK);
	int temp = tempAttack / 5;
	if (temp == 0) temp = 1;
	int tempRand = ((rand() % (temp * 2)) - temp);

	return tempAttack + tempRand;
}