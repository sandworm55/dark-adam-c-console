#include "CombatEvent.h"
#include "sstream"

using std::stringstream;

vector<BaseItem*> CombatEvent::runEvent()
{
	if (_preEvent != nullptr)
		_preEvent->runEvent();

	for each (LifeForm* var in *_party)
	{
		_turnOrder.push_back(var);
	}

	for each (LifeForm* var in _enemies)
	{
		_turnOrder.push_back(var);
	}

	setActive(false);
	while (true)
	{
		getTurnOrder();
		bool enemyDead = true;
		
		for each (LifeForm* var in _turnOrder)
		{
			if (var->isAlive())
			{
				displayFight();
				cout << endl;
				if (var->isPlayerChar())
				{
					char input;
					bool validTurn = true;
					do
					{
						while (true)
						{
							cout << "What would " << var->getName() << " like to do;" << endl;
							cout << "[A]ttack ";
							if (_player->hasItems() && _player->displayNums(BaseItem::ITEM_TYPE::CONSUMABLES)[1] > 0)
								cout << "[I]tem";
							cout << endl << " > ";

							cin >> input;
							input = toupper(input);

							if (input == 'A' || input == 'I')
								break;

							cout << "Invalid choice." << endl;
							Sleep(1000);
							displayFight();
							cout << endl;
						}

						switch (input)
						{
						case 'A':
							validTurn = attack(var);
							break;
						case 'I':
							if (_player->hasItems() && _player->displayNums(BaseItem::ITEM_TYPE::CONSUMABLES)[1] > 0)
								validTurn = item();
							else
								cout << "No items can be used." << endl;
							break;
						default:
							break;
						}
						if (!validTurn)
						{
							displayFight();
							cout << endl;
						}
					} while (!validTurn);
				}
				else if (!var->isPlayerChar())
				{
					int playerNum;

					do
					{
						playerNum = rand() % _party->size();
					} while (!_party->at(playerNum)->isAlive());

					var->attack(_party->at(playerNum));
					//Sleep(500);
				}
				cout << "Continue...";
				system("pause > nul");
			}

			enemyDead = true;
			for each (LifeForm* check in _enemies)
			{
				if (check->isAlive())
					enemyDead = false;
			}
			if (enemyDead)
			{
				displayFight();
				cout << endl;
				cout << "The enemy has been wiped out, good job." << endl;
				break;
			}
			if (_player->partyDead())
			{
				displayFight();
				cout << endl;
				cout << "You have died." << endl;
				cout << "GAME OVER...";
				system("pause > nul");
				break;
			}
		}
		if (_player->partyDead())
			return getRewards();

		if (enemyDead)
		{
			system("pause > nul");
			system("cls");
			break;
		}
	}

	runCallback();
	return getRewards();
}

void CombatEvent::displayFight()
{
	const unsigned char symbol1 = 205;
	const unsigned char symbol2 = 219;

	system("cls");

	for (size_t i = 0; i < _turnOrder.size(); i++)
	{
		// both
		if (i + 1 <= _party->size() && i + 1 <= _enemies.size())
		{
			string tempPlayer = _party->at(i)->getName();
			string tempEnemy = _enemies[i]->getName();

			int playerNameSize = tempPlayer.size() + 2;
			int enemyNameSize = tempEnemy.size();

			stringstream hpPlayer;
			if (_party->at(i)->getCurrentHealth() > 0)
				hpPlayer << " (" << _party->at(i)->getCurrentHealth() << "/" << _party->at(i)->getMaxHealth() << ")";
			else
				hpPlayer << " Wasted";
			stringstream hpEnemy;
			if (_enemies[i]->getCurrentHealth() > 0)
				hpEnemy << " (" << _enemies[i]->getCurrentHealth() << "/" << _enemies[i]->getMaxHealth() << ")";
			else
				hpEnemy << " Wasted";

			int hpPlayerSize = hpPlayer.str().size() + 1;
			int hpEnemySize = hpEnemy.str().size() - 1;

			//// line 1
			cout << symbol2;
			for (int i = 0; i < 23; i++)
			{
				cout << symbol1;
			}
			cout << symbol2;

			cout << "\t\t\t      ";

			cout << symbol2;
			for (int i = 0; i < 23; i++)
			{
				cout << symbol1;
			}
			cout << symbol2 << endl;

			//// line 2
			cout << symbol2 << " " << tempPlayer;

			for (int i = 0; i < 3 - playerNameSize / 8; i++)
			{
				cout << "\t";
			}
			cout << symbol2;

			cout << "\t\t\t      ";

			cout << symbol2 << " " << tempEnemy;

			for (int i = 0; i < 2 - enemyNameSize / 8; i++)
			{
				cout << "\t";
			}
			cout << "      " << symbol2 << endl;

			//// line 3
			cout << symbol2 << hpPlayer.str().c_str();

			for (int i = 0; i < 3 - hpPlayerSize / 8; i++)
			{
				cout << "\t";
			}
			cout << symbol2;

			cout << "\t\t\t      ";

			cout << symbol2 << hpEnemy.str().c_str();

			for (int i = 0; i < 2 - hpEnemySize / 8; i++)
			{
				cout << "\t";
			}
			cout << "      " << symbol2 << endl;

			//// line 4
			cout << symbol2;
			for (int i = 0; i < 23; i++)
			{
				cout << symbol1;
			}
			cout << symbol2;

			cout << "\t\t\t      ";

			cout << symbol2;
			for (int i = 0; i < 23; i++)
			{
				cout << symbol1;
			}
			cout << symbol2 << endl;
		}

		// only player
		if (i + 1 <= _party->size() && i + 1 > _enemies.size())
		{
			string tempPlayer = _party->at(i)->getName();

			int playerNameSize = tempPlayer.size() + 2;

			stringstream hpPlayer;
			if (_party->at(i)->getCurrentHealth() > 0)
				hpPlayer << " (" << _party->at(i)->getCurrentHealth() << "/" << _party->at(i)->getMaxHealth() << ")";
			else
				hpPlayer << " DEAD";

			int hpPlayerSize = hpPlayer.str().size() + 1;

			//// line 1
			cout << symbol2;
			for (int i = 0; i < 23; i++)
			{
				cout << symbol1;
			}
			cout << symbol2 << endl;

			//// line 2
			cout << symbol2 << " " << tempPlayer;

			for (int i = 0; i < 3 - playerNameSize / 8; i++)
			{
				cout << "\t";
			}
			cout << symbol2 << endl;

			//// line 3
			cout << symbol2 << hpPlayer.str().c_str();

			for (int i = 0; i < 3 - hpPlayerSize / 8; i++)
			{
				cout << "\t";
			}
			cout << symbol2 << endl;

			//// line 4
			cout << symbol2;
			for (int i = 0; i < 23; i++)
			{
				cout << symbol1;
			}
			cout << symbol2 << endl;
		}

		// only enemy
		if (i + 1 > _party->size() && i + 1 <= _enemies.size())
		{
			string tempEnemy = _enemies[i]->getName();

			int enemyNameSize = tempEnemy.size();

			stringstream hpEnemy;
			if (_enemies[i]->getCurrentHealth() > 0)
				hpEnemy << " (" << _enemies[i]->getCurrentHealth() << "/" << _enemies[i]->getMaxHealth() << ")";
			else
				hpEnemy << " DEAD";

			int hpEnemySize = hpEnemy.str().size() - 1;

			//// line 1
			cout << "\t\t\t\t\t\t      ";

			cout << symbol2;
			for (int i = 0; i < 23; i++)
			{
				cout << symbol1;
			}
			cout << symbol2 << endl;

			//// line 2
			cout << "\t\t\t\t\t\t      ";

			cout << symbol2 << " " << tempEnemy;

			for (int i = 0; i < 2 - enemyNameSize / 8; i++)
			{
				cout << "\t";
			}
			cout << "      " << symbol2 << endl;

			//// line 3
			cout << "\t\t\t\t\t\t      ";

			cout << symbol2 << hpEnemy.str().c_str();

			for (int i = 0; i < 2 - hpEnemySize / 8; i++)
			{
				cout << "\t";
			}
			cout << "      " << symbol2 << endl;

			//// line 4
			cout << "\t\t\t\t\t\t      ";

			cout << symbol2;
			for (int i = 0; i < 23; i++)
			{
				cout << symbol1;
			}
			cout << symbol2 << endl;
		}
	}
}

bool CombatEvent::attack(LifeForm* attacker)
{
	displayFight();

	vector<LifeForm*> temp;
	for (size_t i = 0; i < _enemies.size(); i++)
	{
		temp.push_back(_enemies[i]);
	}

	int num = 0;
	do
	{
		cout << endl << "Who do you want to attack;" << endl;

		for (size_t i = 0; i < temp.size(); i++)
		{
			if (!(temp[i]->isAlive()))
			{
				temp.erase(temp.begin() + i);
				i -= 1;
			}
			else
				cout << "[" << i + 1 << "]" << temp[i]->getName() << " ";
		}
		cout << "[0]Cancel ";
		cout << endl << "> ";

		if (!(cin >> num) || num < 0 || num >(int)temp.size())
		{
			cin.clear();
			cin.ignore(1024, '\n');
			cout << "Invalid choice." << endl;
			Sleep(1000);
			cout << endl << endl;
		}
		else if (num == 0)
			return false;
		else
			break;

	} while (true);
	attacker->attack(temp[num - 1]);
	return true;
}

bool CombatEvent::item()
{
	vector<int> itemRange = _player->displayNums(BaseItem::ITEM_TYPE::CONSUMABLES);

	system("cls");
	_player->displayInventory(BaseItem::ITEM_TYPE::CONSUMABLES, itemRange[0], itemRange[1]);
	cout << "What will you use?" << endl;
	cout << "> ";

	int itemChoice = -1;
	do
	{
		if (!(cin >> itemChoice) || itemChoice < 0 || itemChoice > itemRange[1])
		{
			cin.clear();
			cin.ignore(UINT_MAX, '\n');
			cout << "Invalid choice." << endl;
			Sleep(1000);
			system("cls");
			continue;
		}
		else if (itemChoice == 0)
			return false;
		else
			break;
	} while (true);

	displayFight();

	itemChoice += itemRange[0];

	BaseItem* item = _player->getInventoryItem(itemChoice);

	cout << item->getName() << " Heals : " << item->getConsumableEffect() << endl;
	cout << "Who would you like to use the " << item->getName() << " on?" << endl;
	for (size_t i = 0; i < _party->size(); i++)
	{
		cout << "[" << i + 1 << "] " << _party->at(i)->getName() << " ";
	}
	cout << endl << "> ";

	int charChoice = -1;
	do
	{
		if (!(cin >> charChoice) || charChoice < 0 || charChoice > _party->size())
		{
			cin.clear();
			cin.ignore(UINT_MAX, '\n');
			cout << "Invalid choice." << endl;
			Sleep(1000);
			system("cls");
			continue;
		}
		else if (itemChoice == 0)
			return false;
		else
			break;
	} while (true);

	_player->getCharacter(charChoice)->useConsumable(_player->getCharacter(charChoice), item);
	return true;
}

void CombatEvent::getTurnOrder()
{
	LifeForm* temp;

	for (size_t i = 0; i < _turnOrder.size(); i++)
	{
		for (size_t k = i + 1; k < _turnOrder.size(); k++)
		{
			if (_turnOrder[i]->getStat(Equipment::STATS::SPEED) < _turnOrder[k]->getStat(Equipment::STATS::SPEED))
			{
				temp = _turnOrder[i];
				_turnOrder[i] = _turnOrder[k];
				_turnOrder[k] = temp;
			}
		}
	}
}

