#ifndef BASE_ITEM_H
#define BASE_ITEM_H

#include <string>
#include <iostream>
#include <vector>

using std::string;
using std::vector;
using std::cout;
using std::cin;
using std::endl;

class BaseItem
{
protected:
	string _name;
	string _description;
	int _itemType;
	int _slot = -1;
	int _stats[4];
	int _stackSize = 0;
	bool _tarAlive = true;;
	int _health = 0;
public:
	BaseItem(string name = "Pocket Sand", string des = "A fist of dust.", int itemType = BASE) :
		_name(name), _description(des), _itemType(itemType)
	{
		if (itemType <= BASE || itemType >= ITEM_TYPE::COUNT)
			_itemType = BASE;
		else
			_itemType = itemType;
	}
	enum ITEM_TYPE { BASE, CONSUMABLES, WEAPON, ARMOR, COUNT };

	string getDescription() const { return _description; }
	string getName() const { return _name; }
	int getSlot() const { return _slot; }
	int getItemType() const { return _itemType; }

	//equipment
	int getStat(int stat) const{ return _stats[stat]; }

	///consumables
	int getStackSize() { return _stackSize; }
	void addStack() { _stackSize += 1; }
	void removeStack() { _stackSize -= 1; if (getStackSize() == 0) delete this; }
	bool getTargetAlive() { return _tarAlive; }
	int getConsumableEffect() const { return _health; }

	void setDescription(string des) { _description = des; }
};

#endif