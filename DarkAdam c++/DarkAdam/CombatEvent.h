#ifndef COMBAT_EVENT_H
#define COMBAT_EVENT_H

#include "Event.h"
#include "Consumable.h"
#include "Player.h"
#include "LifeForm.h"
#include "Character.h"
#include "Equipment.h"
#include "BaseItem.h"
#include <string>
#include <iostream>
#include <Windows.h>

using std::string;
using std::cin;

class CombatEvent : public Event
{
private:
	vector<LifeForm*> _enemies;
	vector<Character*> *_party;
	vector<LifeForm*> _turnOrder;

	Event* _preEvent;

	Player* _player;
public:
	CombatEvent(string name, vector<BaseItem*> items, vector<LifeForm*> enemies, vector<Character*>* party, Player* player, bool active = true, Event* postEvent = nullptr, Event* preEvent = nullptr) :
		Event(name, items, active, Event::COMBAT, postEvent), _enemies(enemies), _party(party),  _player(player), _preEvent(preEvent)
	{}

	vector<BaseItem*> runEvent();
	void getTurnOrder();
	bool attack(LifeForm* attacker);
	bool item();
	void displayFight();
};

#endif