#include "Armor.h"

Armor::Armor(string name, string description,int itemType, int slot, int effect) :
Equipment(name, description, ITEM_TYPE::ARMOR, checkSlot(slot), effect)
{}

int Armor::checkSlot(int slot)
{
	if (slot > NOSLOT && slot < WEAPON)
		return slot;
	else
		return NOSLOT;
}