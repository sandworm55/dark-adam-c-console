#ifndef	CHARACTER_H
#define CHARACTER_H

#include "LifeForm.h"
#include "BaseItem.h"
#include "Equipment.h"

#include <string>

using std::string;
class Character : public LifeForm
{
protected:
	Equipment* _equipment[Equipment::COUNT];

	int _healthBonus = 0;
	int _attackBonus = 0;
	int _defenceBonus = 0;
	int _speedBonus = 0;
public:
	Character(string name, int level, int maxHealth, int attack, int defence, int speed, bool isAlive, bool isPlayerChar = true);
	~Character();

	Equipment* equipItem(Equipment* item);
	Equipment* removeItem(int index);

	string getItemName(int slot) const;
	
	void displayEquipment();
	bool hasEquipment();
	void displayStats();
	void updateStats();

	int getCurrentHealth() const { return _currentHealth + _healthBonus; }
	int getMaxHealth() const { return _maxHealth + _healthBonus; }
	void useConsumable(Character* character, BaseItem* item);

	bool isAlive() const { return !(getCurrentHealth() <= 0); }

	void attack(LifeForm* LifeForm);
	int getStat(int stat);

private:

};

#endif
