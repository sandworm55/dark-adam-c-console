#ifndef CONSUMABLE_H
#define CONSUMABLE_H

#include <iostream>
#include <vector>
#include "BaseItem.h"

using std::vector;
using std::cout;
using std::endl;
using std::cin;

class Consumable : public BaseItem
{
public:
	Consumable(string name, string description, int itemType, int stackSize = 1, int health = 0, bool targetMustBeAlive = true) :
		BaseItem(name, description, itemType)
	{
		_stackSize = stackSize;
		_health = health;
		_tarAlive = targetMustBeAlive;
	}
};


#endif