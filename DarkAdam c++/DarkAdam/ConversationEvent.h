#include "Event.h"
#include <string>
#include <iostream>
#include <Windows.h>

#ifndef CONVERSATION_EVENT_H
#define CONVERSATION_EVENT_H

using std::string;
using std::cout;

class ConversationEvent : public Event
{
private:
	vector<string*> _conversation;
public:
	ConversationEvent(string name, vector<BaseItem*> items, vector<string*> conversation, bool active = true, Event* evnt = nullptr) :
		Event(name, items, active,Event::CONVO, evnt), _conversation(conversation)
	{}

	vector<BaseItem*> runEvent();
};

#endif