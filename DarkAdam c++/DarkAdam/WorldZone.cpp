#include "WorldZone.h"
#include "BaseItem.h"
#include "Equipment.h"
#include "Consumable.h"
#include "LifeForm.h"
#include "Character.h"
#include "InteractionEvent.h"
#include "CombatEvent.h"
#include "ConversationEvent.h"
#include "ActivationEvent.h"
#include "CharAddEvent.h"

#include <iostream>
#include <Windows.h>

using std::cout;
using std::cin;
using std::endl;

bool cinCheck(int& input, int min, int max)
{
	if (!(cin >> input) || input < min || input > max)
	{
		cin.clear();
		cin.ignore(UINT_MAX, '\n');
		cout << "Invalid choice." << endl;
		Sleep(1000);
		system("cls");
		return true;
	}
	else
		return false;
}

WorldZone::WorldZone() :
_currentArea(nullptr), _player(nullptr)
{
	init();
}

WorldZone::~WorldZone()
{
	map<string, Area*>::iterator it = _world.begin();

	while (it != _world.end())
	{
		delete it->second;
		it++;
	}
}

bool WorldZone::addArea(string key, Area* area)
{
	if (_world.find(key) != _world.end())
	{
		return false;
	}

	_world.insert(std::pair<string, Area*>(key, area));
	return true;
}

void WorldZone::setArea(Area* area)
{
	if (area != nullptr)
	{
		_currentArea = area;
	}
}

void WorldZone::linkArea(string key1, string key2)
{
	Area* temp1 = _world.find(key1)->second;
	Area* temp2 = _world.find(key2)->second;

	_world[key1]->setAreaExit(temp1->_exits.size() + 1, _world[key2]);
	_world[key2]->setAreaExit(temp2->_exits.size() + 1, _world[key1]);
}

Area* WorldZone::operator[](string key)
{
	return _world[key];
}

void WorldZone::init()
{
	addArea("BAR", new Area("The Bar"));
	addArea("GANG", new Area("Gang Hideout"));

	//linkArea("BAR", "GANG");

	// The room named "MAIN" will be the entrance when changing areas
	// a room with access to leaving areas needs "true" at the end 
	//---////////////////////////////////////////////////////////////////
	_world["BAR"]->addRoom("MAIN", new Room("The main lobby",
		"\tThe main lounge is a spacious room, with several large tables\n"
		"\tspread out across the room, and booths lining the perimeter of\n"
		"\tthe east wall. You notice that a couple of the tables have small\n"
		"\tgroups of patrons, most of whom are older men and women, slowly\n"
		"\tnursing opaque glasses. The bar to your left is largely empty,\n"
		"\tsave for the man behind it wiping down the counter.", true));
	_world["BAR"]->addRoom("BATH", new Room("The washroom",
		"\tA small with no more then enough space for 2 or 3 people, but\n"
		"\tit's rather clean for being a bars washroom. The owner of this\n"
		"\tplace must really care for it."));
	_world["BAR"]->addRoom("STORE", new Room("Storage room",
		"\tThe handle turns, and the door easily opens in. This is a small\n"
		"\troom, crowded with metal frame shelves and large boxes. There\n"
		"\tis a small light hanging from a chain, and with an audible click,\n"
		"\tyou pull the chain and cast shadowy light across the store room."));
	_world["BAR"]->addRoom("OFFICE", new Room("Back office",
		"\tA personal office and probably a place you shouldn't be in.\n"
		"\tThe back wall is lined with bookselfs and display cases with a\n"
		"\theavy desk in front of them."));

	_world["BAR"]->linkExits(("MAIN"), ("BATH"));
	_world["BAR"]->linkExits(("MAIN"), ("STORE"));
	_world["BAR"]->linkExits(("MAIN"), ("OFFICE"));

	_world["GANG"]->addRoom("MAIN", new Room("The main entrance",
		"\tThe front room of this building was a lobby a one point in time,\n"
		"\tbut has since been abandoned for quite some time. A thick film of\n"
		"\tgrime coats most of the surfaces, and stale sweat is thick in the air.", true));
	_world["GANG"]->addRoom("TRAIN", new Room("A weight room",
		"\tThe door opens as the stench of body odor becomes much stronger.\n"
		"\tThe room is lined with heavy exercise equipment and the floor is\n"
		"\tlittered with free weights. Several men stand around one who is\n"
		"\tbench pressing an impressive amount of weight."));
	_world["GANG"]->addRoom("STORE", new Room("Storage room",
		"\tA storage room that looks like it isn't being used to store anything\n"
		"\tanymore, it is filled with empty metal shelfs.  And has a non-cannon\n"
		"\twindow you can leave through.", true));
	_world["GANG"]->addRoom("HALL", new Room("Hallway",
		"\tThe door leads down a hall way which has two locked doors that\n"
		"\twon't budge and one door at the end that leads upstairs."));
	_world["GANG"]->addRoom("LEAD", new Room("The leaders room.",
		"\tA main area that was once used as a conference room. Most of\n"
		"\tthe chairs are pushed up against the walls, and a small group\n"
		"\tof men stand drinking around a radio in the center of the room."));

	_world["GANG"]->linkExits(("MAIN"), ("TRAIN"));
	_world["GANG"]->linkExits(("MAIN"), ("STORE"));
	_world["GANG"]->linkExits(("MAIN"), ("HALL"));
	_world["GANG"]->linkExits(("HALL"), ("LEAD"));

	vector<BaseItem*> temp;
	vector<LifeForm*> battle;
	vector<string*> convo;
	vector<string*> convo2;
	vector<Event*> evnt;

	LifeForm* _enemyDude;

	temp.push_back(new Equipment("Mop", "A mop found in a bathroom, it looks clean enough but what you dont know won't\n"
		" hurt you... atleast not to much", BaseItem::WEAPON, Equipment::WEAPON, 3, 3, 0, 0));
	_world["BAR"]->_area["BATH"]->addEvent(new InteractionEvent("Cleaning supplies", temp, "\tYou find a mop in a bucket, it has a strong handle and is\n"
		"\tmostly clean."));

	temp.erase(temp.begin(), temp.end());
	{
		temp.push_back(new Equipment("Extension Cord", "An extension cord that is only about six feet in length,\n"
			"but surprisingly heavy and thick, would suck to get hit with",
			BaseItem::WEAPON, Equipment::WEAPON, 3, -1, 2, 0));
	}
	_world["BAR"]->_area["STORE"]->addEvent(new InteractionEvent("Crate", temp, "\tYou find an extension cord. It is only about six feet in length,\n"
		"\tbut surprisingly heavy and thick."));

	temp.erase(temp.begin(), temp.end());
	{
		temp.push_back(new Consumable("Rotgut Whiskey", "It's pretty bad, but it'll do the trick.", BaseItem::CONSUMABLES, 1, 15));
	}
	_world["BAR"]->_area["STORE"]->addEvent(new InteractionEvent("Cardboard Box", temp, "\tAt the bottom of a damp cardboard box, you find a dusty bottle\n"
		"\tof Whiskey."));

	temp.erase(temp.begin(), temp.end());
	{
		temp.push_back(new Equipment("Suit jacket", "A red suit jacket found in the office of Jacks bar", BaseItem::ARMOR, Equipment::CHEST, 0, 2, 1, 2));
	}
	_world["BAR"]->_area["OFFICE"]->addEvent(new InteractionEvent("Coat Rack", temp, "\tA red suit jacket hanging from a coat rack in the corner of the\n"
		"\troom."));

	temp.erase(temp.begin(), temp.end());
	{
		temp.push_back(new Equipment("Dumbbell", "A nice metal weight is on the floor, this will make a great\n"
			"\tweapon.", BaseItem::WEAPON, Equipment::WEAPON, 7, 2, -3, 0));
		temp.push_back(new Equipment("Sparing Gloves", "A pair of well used, tough, lether gloves.", BaseItem::ARMOR, Equipment::HANDS, 0, 1, 0, 2));
	}
	_world["GANG"]->_area["TRAIN"]->addEvent(new InteractionEvent("Equipment rack", temp, "\tA solid metal 5 pound dumbbell and gloves. Only slightly\n"
		"\tsweaty."));

	temp.erase(temp.begin(), temp.end());
	{
		temp.push_back(new Consumable("Juice bottle", "A small room temperature bottle of apple cider", BaseItem::CONSUMABLES, 6, 5));
	}
	_world["GANG"]->_area["STORE"]->addEvent(new InteractionEvent("Small cardboard Box", temp, "\tYou find 6 small bottles of warm juice."));

	/*temp.erase(temp.begin(), temp.end());
	{
	temp.push_back(new Equipment("Hat", "A hat", BaseItem::ARMOR, Equipment::HEAD, 0, 1, 1, 0));
	temp.push_back(new Equipment("Glove", "A glove", BaseItem::ARMOR, Equipment::HANDS, 0, 1, 1, 0));
	temp.push_back(new Equipment("Shirt", "A shirt", BaseItem::ARMOR, Equipment::CHEST, 0, 1, 1, 0));
	temp.push_back(new Equipment("Pants", "A pants", BaseItem::ARMOR, Equipment::LEGS, 0, 1, 1, 0));
	temp.push_back(new Equipment("Shoes", "A shoes", BaseItem::ARMOR, Equipment::FEET, 0, 1, 1, 0));
	temp.push_back(new Consumable("Coin", "A coin", BaseItem::BASE, 1, 0));
	temp.push_back(new Consumable("Coin", "A coin", BaseItem::BASE, 1, 0));
	temp.push_back(new Consumable("Coin", "A coin", BaseItem::BASE, 1, 0));
	temp.push_back(new Consumable("Coin", "A coin", BaseItem::BASE, 1, 0));
	temp.push_back(new Equipment("Plate", "A plate", BaseItem::WEAPON, Equipment::WEAPON, 1, 5, 0, 0));
	temp.push_back(new Equipment("Knife", "A knife", BaseItem::WEAPON, Equipment::WEAPON, 3, 0, 3, 0));
	temp.push_back(new Equipment("Fork", "A fork", BaseItem::WEAPON, Equipment::WEAPON, 3, 0, 3, 0));
	temp.push_back(new Equipment("Spoon", "For scooping", BaseItem::WEAPON, Equipment::WEAPON, 2, 1, 2, 0));
	temp.push_back(new Consumable("Consumable", "It is a lie, also does nothing", BaseItem::CONSUMABLES, 3, 10));
	}
	_world["BAR"]->_area["MAIN"]->addEvent(new InteractionEvent("Test Loot", temp, "A box of items."));*/

	temp.erase(temp.begin(), temp.end());
	_world["BAR"]->_area["OFFICE"]->addEvent(new InteractionEvent("Desk", temp, "\tThere is a small picture on the corner of the desk. It\n"
		"\tshows Adam's old gang of freinds, before everything went to hell."));

	_currentArea = _world["BAR"];
	_currentArea->setRoom(_currentArea->_area["MAIN"]);

	_player = new Player();
	_player->addCharacter(new Character("Adam", 1, 30, 10, 6, 8, true));

	// Patron Conversation
	convo.push_back(new string("\tYou approach an elderly man sitting by himself, sipping on a\n"
		"\tdrink from a dark mug."));
	{
		convo.push_back(new string("Adam :\tHey there friend, mind if I join you for a drink?"));
		convo.push_back(new string("Man :\tHmpf."));
		convo.push_back(new string("Adam :\tMy name is Adam, I used to live around these parts. And you are...?"));
		convo.push_back(new string("Man :\tHenry, if you was from here and left, why would you come back?"));
		convo.push_back(new string("Adam :\tNo place like home, right?"));
		convo.push_back(new string("Henry :\tHmpf."));
		convo.push_back(new string("Adam :\tAnd either way, I'm just in town to visit some... old friends. Speaking\n"
			"\tof, have you seen Jack around? I heard this was his place."));
		convo.push_back(new string("Henry :\tHe walked past me out back a bit ago. Some drunk was causing problems, \n"
			"\tJack took him out back."));
		convo.push_back(new string("Adam :\tI see. Thanks Henry, hope you have a good night."));
		convo.push_back(new string("Henry :\tHmpf."));
	}
	_world["BAR"]->_area["MAIN"]->addEvent(new ConversationEvent("Patron", temp, convo));
	convo.erase(convo.begin(), convo.end());

	// Aggueeing couple
	convo.push_back(new string("\tYou approach a couple having a quiet yet heated conversation."));
	{
		convo.push_back(new string("Adam :\tI better not interject, this seems personal. But I am kind of curious.."));
		convo.push_back(new string("Man :\tLook, I don't what you expect me to do."));
		convo.push_back(new string("Woman :\tI expect you to take the offer! My Dad is offering you a job upstate,\n"
			"\tthis is our chance to get out of this city!"));
		convo.push_back(new string("Man :\tAnd I just leave my mom and little sister here?"));
		convo.push_back(new string("Woman :\tYes! I understand you want to be here for them but what about us? Do\n"
			"\tyou want to spend the rest of our lives in this place?"));
		convo.push_back(new string("Man :\tThat isn't fair."));
		convo.push_back(new string("Woman :\tI know."));
		convo.push_back(new string("Adam :\tGod this place never changes..."));
	}
	_world["BAR"]->_area["MAIN"]->addEvent(new ConversationEvent("Couple", temp, convo));
	convo.erase(convo.begin(), convo.end());



	_enemyDude = new LifeForm("Thug 1", 1, 15);
	battle.push_back(_enemyDude);
	_enemyDude = new LifeForm("Thug 2", 1, 15);
	battle.push_back(_enemyDude);
	convo.push_back(new string(
		"\tFrom across the bar, you see a young man walk in from the door\n"
		"\tlabeled \"Employees Only\". He wears a red pin striped jacket\n"
		"\tand matching red pants. He sweats as he walks across the room,\n"
		"\tand takes a heavy seatat the bar, motioning to the bartender\n"
		"\tfor a drink."));

	{
		convo.push_back(new string("Adam :\tWell, here we go."));

		convo.push_back(new string("\tYou take a seat at the bar next to the man."));

		convo.push_back(new string("Adam :\tWhiskey, neat please."));
		convo.push_back(new string("Bartender : Liquor is illegal in this city sir."));
		convo.push_back(new string("Adam :\tSo what do you people drink?"));
		convo.push_back(new string("Bartender : Depends on who's asking. Who let you inside?"));
		convo.push_back(new string("Adam :\tThat charming fellow at the door.  Said he wanted to stare at me all\n"
			"\tnight."));
		convo.push_back(new string("Bartender : This isn't the place to be funny. Fuck off."));
		convo.push_back(new string("Jack :\tServe him Saul, he isn't a cop."));
		convo.push_back(new string("Saul :\tI don't give a rat's ass, I don't know him, he isn't drinking."));
		convo.push_back(new string("Jack :\tI do, give the man a drink."));
		convo.push_back(new string("Adam :\tYou heard him Saul, and make sure it's stiff."));
		convo.push_back(new string("Saul :\tDon't get cute."));
		convo.push_back(new string("Adam :\tI surprised you're so happy to see me Jack. Mike was convinced you\n"
			"\twouldn't speak to me."));
		convo.push_back(new string("Jack :\tYou have until you finish that drink to spill your guts."));
		convo.push_back(new string("Adam :\tOh, is that right? What makes you think I didn't just stop in for a\n"
			"\tdrink?"));
		convo.push_back(new string("Jack :\tBecause you've been dead for five years.  Ghosts don't usually stop in\n"
			"\tfor a friendly chat, so I'd like to know why you're haunting me before\n"
			"\tI kick your ass."));
		convo.push_back(new string("Adam :\tEasy there champ, or did you forget who the better fighter always\n"
			"\twas, \"Little Jackie\"."));
		convo.push_back(new string("Jack :\tFive years is a long time Adam, I'm not so little anymore.  I'm giving\n"
			"\ta chance to talk, I'd suggest you take it."));
		convo.push_back(new string("Adam :\tAlright tough guy.  Look at you, calling shots, bootlegging liquor,\n"
			"\treal big time gangster these days eh Jack? I didn't think you'd ever\n"
			"\thave it in you."));
		convo.push_back(new string("Jack :\tThat'd be something you gave me, Adam, when you decided to ruin our\n"
			"\tlives 5 years ago. When you decided you wanted to abandon us...\n"
			"\tDesperate times can call for desperate measures. I'm sure you're\n"
			"\tfamiliar with that."));
		convo.push_back(new string("Saul :\tDo you want me to throw this jackass out Jack?"));
		convo.push_back(new string("Adam :\tYea Jack, do you?"));
		convo.push_back(new string("Jack :\tI'm still waiting for something to fall out of your mouth that isn't\n"
			"\tsarcastic."));
		convo.push_back(new string("Adam :\tAlright, fine. I'm sure you can guess why I'm here. I'm looking for\n"
			"\tSybil. I have a lead on her, and I need your help."));
		convo.push_back(new string("Jack :\tA lead?"));
		convo.push_back(new string("Adam :\tYea, just a lead.  I've heard someone matching her description has been\n"
			"\tseen with Arete. It isn't much, but it is something."));
		convo.push_back(new string("Jack :\tSo, let me be clear here.  You come back after five years, five years\n"
			"\twithout so much as a phone call.  You come into my bar, you insult me,\n"
			"\tyou insult Saul, and then you ask me to help you take on the largest\n"
			"\torganized crime group in the city to help you find someone I hate.\n"
			"\tI just want to make sure I didn't take a blow to the head earlier."));
		convo.push_back(new string("Adam :\tThat sounds about right."));
		convo.push_back(new string("Jack :\tGreat, great. Absolutely not. Get out of my bar."));
		convo.push_back(new string("Adam :\tJack wait a minute..."));

		convo.push_back(new string("\tCrashing sounds"));

		convo.push_back(new string("Thug 1 : Where the fuck is he?"));
		convo.push_back(new string("Thug 2 : You messed with the wrong guy today, asshole. There, he's at the bar!"));
		convo.push_back(new string("Adam :\tFriends of yours Jack?"));
		convo.push_back(new string("Jack :\tSomething like that. Excuse me for a second."));
		convo.push_back(new string("Adam :\tI'm getting in on this too, it's been too long."));

		convo2.push_back(new string("Thug 1 : This won't be the last of this Jack!"));
		convo2.push_back(new string("Thug 2 : This is our neighborhood whether you like it or not!"));

		convo2.push_back(new string("\tThe Thugs flee"));
		convo2.push_back(new string("Adam :\tHuff . . Huff . . You weren't lying, seems like you learned how to\n"
			"\tthrow a kick."));
		convo2.push_back(new string("Jack :\tYou look out of shape. You should really get some exercise in your old\n"
			"\tage."));
		convo2.push_back(new string("Adam :\tWheeze . . Oh, I just need to get back into the swing of . . Huff . .\n"
			"\tthings."));

		convo2.push_back(new string("\tYou return to your seat at the bar and find a fresh drink."));
		convo2.push_back(new string("Adam :\tSo Jack, like I was trying to say..."));
		convo2.push_back(new string("Jack :\tAdam, there is no god damn way I'm going to help you."));
		convo2.push_back(new string("Adam :\t....Just like that? That's it?"));
		convo2.push_back(new string("Jack :\tJust like that. That's it."));
		convo2.push_back(new string("Adam :\tI don't even get an excuse?"));
		convo2.push_back(new string("Jack :\tAn excuse? Do I need a reason Adam?"));
		convo2.push_back(new string("Adam :\tWell I...."));
		convo2.push_back(new string("Jack :\tNo, don't even start, I'll give you a fucking excuse. I'll give you the\n"
			"\tsame excuse you gave us when you decided we were pulling that job. You\n"
			"\tknow, the one that was clearly a trap?"));
		convo2.push_back(new string("Adam :\t...."));
		convo2.push_back(new string("Jack :\tAnd I'll give you the same excuse you gave us when that job, the\n"
			"\tone we just talked about being a trap, went to high hell, and we all \n"
			"\talmost died. It's the same excuse you had when Syb up and vanished \n"
			"\twithout a murmur or a trace."));
		convo2.push_back(new string("Adam :\tHey listen...."));
		convo2.push_back(new string("Jack :\tFunnily enough Adam, it's the same fucking excuse you gave us when you\n"
			"\tdied. And we buried you. It's what you said to Mack when he helped\n"
			"\tlower your coffin, and the response you gave when The Poet read your\n"
			"\teulogy. Do you remember what you told Edward when he was covering your\n"
			"\tgrave?"));
		convo2.push_back(new string("Adam :\t..."));
		convo2.push_back(new string("Jack :\tFUCKING NOTHING ADAM! NOT A GOD DAMN SYLLABLE!"));
		convo2.push_back(new string("Saul :\tJack, you need to calm down."));
		convo2.push_back(new string("Adam :\t..."));
		convo2.push_back(new string("Jack :\t..."));
		convo2.push_back(new string("Saul :\tI think you should go."));
		convo2.push_back(new string("Adam :\tAren't you at least curious, Jack?"));
		convo2.push_back(new string("Jack :\tAbout what?"));
		convo2.push_back(new string("Adam :\tAbout what happened that night."));
		convo2.push_back(new string("Jack :\tI know what happened that night, I was there."));
		convo2.push_back(new string("Adam :\tNo, you have an idea. Maybe a strong idea. But you don't actually know.\n"
			"\tI'm not expecting you to forgive me Jack. I guess, I don't even really\n"
			"\texpect you to like me. I had my reasons for what I did, you know that.\n"
			"\tI'm not proud of how things played out, but I can't go back and change\n"
			"\tit."));
		convo2.push_back(new string("Jack :\t..."));
		convo2.push_back(new string("Adam :\tI'm going to look for Syb, with or without you. I came here because I\n"
			"\tthought I owed you at least that much. Saul, appreciate all the\n"
			"\thospitality."));

		convo2.push_back(new string("\tAdam gets up to leave"));

		convo2.push_back(new string("Jack :\tYou're leaving?"));
		convo2.push_back(new string("Adam :\tAs much as I'd love to hang around and get drunk, I can't. I need to\n"
			"\tgo find Edward and The Poet. Stay safe Jackie."));

		convo2.push_back(new string("\tAdam leaves"));

		convo2.push_back(new string("Jack :\tAnother round Saul."));
		convo2.push_back(new string("Saul :\tYou aren't going after him?"));
		convo2.push_back(new string("Jack :\tWhy would I do that?"));
		convo2.push_back(new string("Saul :\tI'm not your Dad kid, and I won't tell you what to do. But, seems to me\n"
			"\tthat if you're upset about Adam abandoning you, you're no better than\n"
			"\the is right now."));
		convo2.push_back(new string("Jack :\t....God damnit. Keep on eye on the place for a while Saul."));

		convo2.push_back(new string("\tJack approaches."));

		convo2.push_back(new string("Jack :\tTwo conditions Adam."));
		convo2.push_back(new string("Adam :\tSure."));
		convo2.push_back(new string("Jack :\tOne. When we find Syb, you need to understand we are looking for her\n"
			"\tfor different reasons. I want the truth, and then I want retribution."));
		convo2.push_back(new string("Adam :\tAnd number two?"));
		convo2.push_back(new string("Jack :\tI need help with a bit of a favor..."));
	}

	evnt.push_back(new ConversationEvent("Jack", temp, convo, true,
		new CharAddEvent("", temp, new Character("Jack", 1, 30, 13, 7, 5, true), _player,
		new CombatEvent("Thugs", temp, battle, _player->getPartyPointer(), _player, true,
		new ConversationEvent("Jack-cont", temp, convo2, true)))));

	_world["BAR"]->_area["MAIN"]->addActiveEvent(new ActivationEvent(evnt, getArea("BAR"), getArea("GANG"), temp));
	_world["BAR"]->_area["MAIN"]->addEvent(evnt[0]);

	evnt.erase(evnt.begin(), evnt.end());
	convo.erase(convo.begin(), convo.end());
	battle.erase(battle.begin(), battle.end());
	convo2.erase(convo2.begin(), convo2.end());

	convo.push_back(new string("\tKnock"));
	{
		convo.push_back(new string("Occupent : Occupied"));
		convo.push_back(new string("\tKnock again"));
		convo.push_back(new string("Occupent : I said it's busy pal"));
		convo.push_back(new string("\tKnock furiously"));
		convo.push_back(new string("\tThe door flies open."));
		convo.push_back(new string("Man :\tAre you about to piss yourself?!"));
		convo.push_back(new string("Adam :\tNot especially, but I thought there might be some neat loot in here."));
		convo.push_back(new string("Man :\tWhat the hell are you talking about?!"));
		convo.push_back(new string("Adam :\tI'm not entirely sure, I'm leaving."));
	}
	_world["BAR"]->_area["BATH"]->addEvent(new ConversationEvent("Closed Stall", temp, convo, true));
	convo.erase(convo.begin(), convo.end());

	_enemyDude = new LifeForm("Punk w/Bat", 1, 20, 15, 6);
	{
		battle.push_back(_enemyDude);
		_enemyDude = new LifeForm("Punk w/Helmet", 1, 20, 10, 8);
		battle.push_back(_enemyDude);

		temp.erase(temp.begin(), temp.end());
		temp.push_back(new Equipment("Helmet", "A helmet to protect your brain shell.",	BaseItem::ARMOR, Equipment::HEAD, 0, 1, 0, 5));

		convo.push_back(new string("\tThe front room of this building was a lobby a one point in time,\n"
			"\tbut has since been abandoned for quite some time. A thick film of\n"
			"\tgrime coats most of the surfaces, and stale sweat is thick in the air."));
		convo.push_back(new string("Adam :\tGod this place is a dump. Why are we here again?"));
		convo.push_back(new string(
			"Jack :\tI told you, some of the local gutter kids have gotten together to\n"
			"\tform a makeshift gang. Nothing serious like Arete, but I don't want\n"
			"\tthem thinking they can get established, especially if I'm leaving for a\n"
			"\twhile."));
		convo.push_back(new string("Adam :\tSo, what do you want to do?"));
		convo.push_back(new string("Jack :\tTeach em a lesson."));
		convo.push_back(new string("Adam :\tWhat lesson is that?"));
		convo.push_back(new string("Jack :\tThe only one punks like this understand."));
		convo.push_back(new string("Punk #1 : Hey, you guys aren't supposed to be in here!"));
		convo.push_back(new string(
			"Punk #2 : That's Jack! We'll teach you not to fuck with us, there's a new\n"
			"\ttop dog in this neighborhood!"));
		convo.push_back(new string("Adam :\tAh, that lesson..."));

	}
	_world["GANG"]->_area["MAIN"]->addCombatEvent(new CombatEvent("Hideout Gaurds", temp, battle, _player->getPartyPointer(), _player, true, nullptr,
		new ConversationEvent("", temp, convo)));
	battle.erase(battle.begin(), battle.end());
	convo.erase(convo.begin(), convo.end());

	_enemyDude = new LifeForm("Jock #1", 2, 25);
	{
		battle.push_back(_enemyDude);
		_enemyDude = new LifeForm("Jock #2", 1, 20, 10, 5);
		battle.push_back(_enemyDude);
		_enemyDude = new LifeForm("Jock #4", 1, 20, 10, 5);
		battle.push_back(_enemyDude);

		convo.push_back(new string(
			"\tThe door opens as the stench of body odor becomes much stronger.\n"
			"\tThe room is lined with heavy exercise equipment and the floor is\n"
			"\tlittered with free weights. Several men stand around one who is\n"
			"\tbench pressing an impressive amount of weight."));
		convo.push_back(new string("Jock #1 : Come on man!"));
		convo.push_back(new string("Jock #2 : You got this man, no fear!"));
		convo.push_back(new string("Jock #3 : Uraaaaaagh."));
		convo.push_back(new string("Jock #4 : Don't be a pussy bro! Just do it!"));
		convo.push_back(new string("Jock #3 : URAAAAAAAAAAAAH!"));

		convo.push_back(new string("Adam :\tIs everything going on in here consensual?"));
		convo.push_back(new string("Jock #1 : Intruders?!"));
		convo.push_back(new string("Jock #2 : Intruders!"));
		convo.push_back(new string("Jock #4 : INTRUDERS!"));
		convo.push_back(new string("Jock #3 : Huh? What?! Hey, someone fucking spot me!"));
		//// Combat
		convo2.push_back(new string("Adam :\tThere's one left."));
		convo2.push_back(new string("Jock #3 : Help Meeeee..."));
		convo2.push_back(new string("Jack :\tIf we do, are going to go home?"));
		convo2.push_back(new string("Jock #3 : I think my clavicle is broken."));
		convo2.push_back(new string("Adam :\tClose enough."));
	}
	_world["GANG"]->_area["TRAIN"]->addCombatEvent(new CombatEvent("Training Room", temp, battle, _player->getPartyPointer(), _player, true,
		new ConversationEvent("", temp, convo2),
		new ConversationEvent("", temp, convo)));
	battle.erase(battle.begin(), battle.end());
	convo.erase(convo.begin(), convo.end());
	convo2.erase(convo2.begin(), convo2.end());

	convo.push_back(new string(
		"Adam :\tHow many guys do think are here Jack? This might get dicey if we have\n"
		"\tto plow through any more meatheads."));
	{
		convo.push_back(new string(
			"Jack :\tNot many I'd imagine. The de facto leader is this snot nosed kid Dylan.\n"
			"\tHis friends call him \"Edge\""));
		convo.push_back(new string("Adam :\tSo, would you say he's.... Edgy?"));
		convo.push_back(new string("Jack :\tGroan . . Just start climbing."));
	}
	_world["GANG"]->_area["HALL"]->addEvent(new ConversationEvent("Whining Adam", temp, convo));
	convo.erase(convo.begin(), convo.end());

	_enemyDude = new LifeForm("Edge", 3, 30, 16, 8);
	{
		battle.push_back(_enemyDude);
		_enemyDude = new LifeForm("Thug 1", 1, 15, 11);
		battle.push_back(_enemyDude);
		_enemyDude = new LifeForm("Thug 2", 1, 15, 11);
		battle.push_back(_enemyDude);

		convo.push_back(new string(
			"\tThe stairs lead up one story, and open up to a main area that\n"
			"\twas once used as a conference room. Most of the chairs are\n"
			"\tpushed up against the walls, and a small group of men stand\n"
			"\tdrinking around a radio in the center of the room."));

		convo.push_back(new string("Man :\tWho's creeping around back there?"));
		convo.push_back(new string("Adam :\tIs that them Jack?"));
		convo.push_back(new string("Jack :\tI think so, it's sort of hard to tell in this light."));
		convo.push_back(new string("Man :\tHey, don't have a conversation like we aren't here!"));
		convo.push_back(new string("Adam :\tLet's just beat on them and leave. I'm getting tired and a bulk of\n"
			"\ttheir gang has to be cripples at this point."));
		convo.push_back(new string("Jack :\tYou baby, how are going to fight Arete if you can't deal with chumps\n"
			"\tlike this."));

		convo.push_back(new string("\tThe main figure steps forward and pulls a razor blade out from his\n"
			"\tsleeve."));

		convo.push_back(new string("Man :\tI'm getting real tired of being ignored. I think I'll let my blade do\n"
			"\tsome talking."));
		convo.push_back(new string("Adam :\tSo, I guess that makes you the EdgeLord?"));
		convo.push_back(new string("Edge :\tYou're goddamn right... Hey wait a minute, are you disrespecting me\n"
			"\tstill?!"));
		convo.push_back(new string("Jack :\tHe was, I heard it. You should cut him."));
		convo.push_back(new string("Edge :\tShut up! No more games! Come on boys, let's take out the trash!"));

	}
	_world["GANG"]->_area["LEAD"]->addCombatEvent(new CombatEvent("Head Quarters", temp, battle, _player->getPartyPointer(), _player, true, nullptr,
		new ConversationEvent("", temp, convo)));
	battle.erase(battle.begin(), battle.end());
	convo.erase(convo.begin(), convo.end());

	convo.push_back(new string(
		"\tA cab rolls to stop outside of a brick building. The gently falling\n"
		"\train rolls down it's aged and weathered facade. A single light\n"
		"\tshines above a large metal door."));
	{
		convo.push_back(new string(
			"\tA figure dressed in a red a coat, hood pulled up, steps from\n"
			"\tthe cab as the driver calls out to him"));

		convo.push_back(new string("Mike :\tI don't know what you're expecting to find in there Adam. Five\n"
			"\tyears is a long time."));
		convo.push_back(new string("Adam :\tWhat, you don't think he'll be thrilled to see me?"));
		convo.push_back(new string("Mike :\tNo, I don't. Don't think he'll be as happy to see you as I am.\n"
			"\tAnd I also don't think he'll be charmed by your trademark sarcasm Adam."));
		convo.push_back(new string("Adam :\tDuly noted. Thanks for the ride Mike. I'll be in touch. I promise."));
		convo.push_back(new string("Mike :\tYou'd better be. I won't attend your funeral a second time kid."));

		convo.push_back(new string("\tThe cab rolls away from the curb"));
		convo.push_back(new string(
			"\tScanning the area, you notice that all of the street lights\n"
			"\tin the area work, casting a bright light and illuminating much\n"
			"\tof the street. The fire hydrants have been freshly painted, and\n"
			"\tthe buildings are surprisingly free from graffiti"));

		convo.push_back(new string("Adam :\tWell, I guess it's now or never"));

		convo.push_back(new string("\tYou step towards the door, and pound several times with the flat of\n"
			"\tyour fist"));
		convo.push_back(new string("\tAfter several moments, the slat on the door opens, and a pair of eyes\n"
			"\tcan be seen, staring intently at you"));

		convo.push_back(new string("Doorman : Can I help you?"));
		convo.push_back(new string("Adam :\tYea, I was in the neighborhood, figured I'd get out of the rain and\n"
			"\tstop in for a drink."));
		convo.push_back(new string("Doorman : You have the wrong place, this is a private residence."));
		convo.push_back(new string("Adam :\tLook, I'm here to see Jack. Let me in."));
		convo.push_back(new string("Doorman : ... Who sent you here?"));
		convo.push_back(new string("Adam :\tA fucking ghost. Are you going to let me in or not?"));
		convo.push_back(new string("\tThe door slowly creaks open as light pours out onto the street"));

		convo.push_back(new string("Doorman : He's out back dealing with some trash.  Don't cause any trouble,\n"
			"\tI've got my eyes on you."));
		convo.push_back(new string("Adam :\tBe sure to get a good look big guy."));
		convo.push_back(new string(
			"\tAs you step inside past the mountain of a man at the door, the\n"
			"\troom before you comes to life as your vision focus in the bright\n"
			"\tlight."));
	}
	ConversationEvent intro("", temp, convo);
	convo.erase(convo.begin(), convo.end());

	intro.runEvent();
	system("CLS");
}

bool WorldZone::running()
{
	_currentArea->showArea(_currentArea);

	if (queryAction() == 'Q')
	{
		return false;
	}

	if (_player->partyDead())
		return false;

	for each (Event* var in _world["GANG"]->_area["LEAD"]->getCombatEvent())
	{
		if (!var->getActive())
		{
			vector<BaseItem*> item;
			vector<string*> text;

			text.push_back(new string("Jack :\tDamn, that kid could actually fight."));
			text.push_back(new string("Adam :\tYou're telling me, I see now why you didn't want them running loose\n"
				"\tin the neighborhood. Do you think they'll be back?"));
			text.push_back(new string("Jack :\tDid we ever let an ass kicking deter us?"));
			text.push_back(new string("Adam :\tPoint taken."));
			text.push_back(new string("Jack :\tI do appreciate your help Adam, and I'm ready when you are now. I\n"
				"\thaven't spoken to Edward or The Poet in years, I'm afraid I won't be\n"
				"\tmuch help finding them."));
			text.push_back(new string("Adam :\tI have a few ideas, but I'm sure they'll be more trouble along the way."));
			text.push_back(new string("Jack :\tAlways is, but if this city hasn't killed them yet, I'm sure you'll\n"
				"\tsniff them out."));
			text.push_back(new string("Adam :\tThanks. I'm glad to have you with me Jack."));
			text.push_back(new string("Jack :\tEasy there lover boy. Let's go to work."));
			ConversationEvent outro("", item, text);

			outro.runEvent();

			cout << "GAME OVER...";
			system("pause > nul");
			return false;
		}
	}

	return true;
}

char WorldZone::queryAction()
{
	char choice = '~';

	cout << endl << endl << endl << "[M]ove ";

	if (_currentArea->_currentRoom->hasAreaExit() && _currentArea->getExits().size() > 0)
	{
		cout << "[L]eave ";
	}

	if (_currentArea->getRoom()->hasEvent())
	{
		cout << "[E]vent ";
	}

	if (_player->hasItems())
	{
		cout << "[I]nventory ";
	}

	cout << "[S]tats " << "[Q]uit";
	cout << endl << "> ";
	cin >> choice;

	choice = toupper(choice);

	switch (choice)
	{
	case 'M':
		_currentArea->move();
		break;
	case 'L':
		if (_currentArea->_currentRoom->hasAreaExit() && _currentArea->getExits().size() > 0)
		{
			move();
		}
		else
		{
			cout << "You can't change areas from here." << endl << endl;
		}
		break;
	case 'E':
		if (_currentArea->getRoom()->hasEvent())
		{
			events();
			system("pause > nul");
		}
		else
		{
			cout << "Nothing to do." << endl;
		}
		break;
	case 'I':
		if (_player->hasItems())
		{
			inventory();
			system("pause > nul");
		}
		else
		{
			cout << "You have nothing." << endl;
		}
		break;
	case 'S':
		stats();
		break;
	case 'Q':
		return choice;
	default:
		cout << "I don't understand." << endl;
	}
	system("cls");
	return choice;
}

void WorldZone::move()
{
	int choice = 0;

	do
	{
		cout << endl << "Which way would you like to go?" << endl;
		_currentArea->listExits();
		cout << "> ";

		if (cinCheck(choice, 0, _currentArea->getExits().size()))
		{
			_currentArea->showArea(_currentArea);
			cout << endl << endl;
		}
		else if (choice == 0)
			return;
		else break;
	} while (true);

	if (move(choice) == false)
	{
		cout << endl << "You can't go that way." << endl;
	}
}

bool WorldZone::move(int index)
{
	if (_currentArea != nullptr)
	{
		if ((int)_currentArea->getExits().size() >= index && index > 0)
		{
			vector<BaseItem*> temp;
			setArea(_currentArea->getExits()[index - 1]);
			temp = _currentArea->setRoom(_currentArea->_area.find("MAIN")->second);
			for each (BaseItem* var in temp)
			{
				_player->pickUpItem(var);
			}
			return true;
		}
	}
	return false;
}

void WorldZone::events()
{
	int choice = -1;

	do
	{
		cout << endl << "What would you like to do?" << endl;
		_currentArea->getRoom()->listEvents();
		cout << "> ";

		if (cinCheck(choice, 0, _currentArea->getRoom()->getEventSize()))
		{
			_currentArea->showArea(_currentArea);
			cout << endl << endl;
		}
		else if (choice == 0) return;
		else break;
	} while (true);

	vector<BaseItem*> temp = _currentArea->getRoom()->runEvent(choice);
	if (temp.size() != 0)
	{
 		if (temp[0] == nullptr)
		{
			cout << "Invalid option." << endl;
		}
		else
		{
			for each (BaseItem* var in temp)
			{
				_player->pickUpItem(var);
			}
		}
	}
}

void WorldZone::inventory()
{
	int userInput = -1;

	do
	{
		cout << endl << "Which category would you like to see?" << endl;
		cout << "[1]Basic [2]Consumable [3]Weapons [4]Equipment [5]All [0]Cancel" << endl;
		cout << "> ";

		if (cinCheck(userInput, 0, 5))
		{
			_currentArea->showArea(_currentArea);
			cout << endl << endl;
		}
		else if (userInput == 0) return;
		else if (userInput > 0 && userInput <= 5) break;
	} while (true);

	int choice = -1;
	vector<int> itemRange = _player->displayNums(userInput - 1);

	do
	{
		cout << endl << endl << "Which item would you like to use?" << endl;
		_player->displayInventory(userInput - 1, itemRange[0], itemRange[1]);
		cout << "> ";

		if (cinCheck(choice, 0, itemRange[1]))
		{
			_currentArea->showArea(_currentArea);
			cout << endl << endl;
			cout << endl << "Which category would you like to see?" << endl;
			cout << "[1]Basic [2]Consumable [3]Weapons [4]Equipment [5]All [0]Cancel" << endl;
			cout << "> " << userInput;
		}
		else if (choice == 0) return;
		else break;
	} while (true);

	choice += itemRange[0];
	BaseItem* temp = _player->getInventoryItem(choice);

	if (temp == nullptr)
	{
		cout << "Invalid option." << endl;
	}
	else
	{
		do
		{
			char interactionType = '~';

			cout << endl << "What would you like to do with it?" << endl;

			if (temp->getItemType() == BaseItem::WEAPON || temp->getItemType() == BaseItem::ARMOR)
			{
				cout << "[E]quip ";
			}

			if (temp->getItemType() == BaseItem::CONSUMABLES)
			{
				cout << "[U]se ";
			}

			cout << "[I]nspect " << "[C]ancel";
			cout << endl << "> ";
			cin >> interactionType;

			interactionType = toupper(interactionType);

			switch (interactionType)
			{
			case 'E':
				if (temp->getItemType() == BaseItem::WEAPON || temp->getItemType() == BaseItem::ARMOR)
				{
					if (equip((Equipment*)temp))
						_player->removeItem(choice);
					break;
				}
				cout << "That item can't be equipped. " << endl;
				Sleep(1000);
				break;
			case 'U':
				if (temp->getItemType() == BaseItem::CONSUMABLES)
				{
					use((Consumable*)temp);
					break;
				}
				cout << "Not a usable item. " << endl;
				Sleep(1000);
				break;
			case 'I':
				cout << temp->getDescription() << endl;

				if (temp->getItemType() == BaseItem::ITEM_TYPE::CONSUMABLES)
				{
					cout << "Heals: " << temp->getConsumableEffect() << endl;
				}
				else if (temp->getItemType() == BaseItem::ITEM_TYPE::WEAPON || temp->getItemType() == BaseItem::ITEM_TYPE::ARMOR)
				{
					cout << "Attack:  " << temp->getStat(Equipment::STATS::ATTACK) << endl;
					cout << "Defence: " << temp->getStat(Equipment::STATS::DEFENCE) << endl;
					cout << "Health:  " << temp->getStat(Equipment::STATS::HEALTH) << endl;
					cout << "Speed:   " << temp->getStat(Equipment::STATS::SPEED) << endl;
				}
				else
					break;

				system("pause > nul");
				continue;
			case 'C':
				return;
			default:
				cout << "Invalid choice." << endl;
				Sleep(1000);
				system("cls");
				_currentArea->showArea(_currentArea);
				cout << endl << endl;
				cout << endl << "Which category would you like to see?" << endl;
				cout << "[1]Basic [2]Consumable [3]Weapons [4]Equipment [5]All [0]Cancel" << endl;
				cout << "> " << userInput;
				cout << endl << endl << "Which item would you like to use?" << endl;
				_player->displayInventory(userInput - 1, itemRange[0], itemRange[1]);
				cout << "> " << choice - itemRange[0];;
				continue;
			}
			break;
		} while (true);
	}
}

void WorldZone::use(Consumable* temp)
{
	vector<int> itemRange = _player->displayNums(BaseItem::ITEM_TYPE::CONSUMABLES);

	cout << "Who would you like to use the " << temp->getName() << " on?" << endl;
	for (size_t i = 0; i < _player->getParty().size(); i++)
	{
		cout << "[" << i + 1 << "] " << _player->getCharacter(i + 1)->getName() << " ";
		cout << " (" << _player->getCharacter(i + 1)->getCurrentHealth() << "/" << _player->getCharacter(i + 1)->getMaxHealth() << ")" << endl;
	}
	cout << "[0] Cancel" << endl;
	cout << "> ";

	int charChoice = -1;
	do
	{
		if (cinCheck(charChoice, 0, (int)_player->getParty().size()))
		{
			continue;
		}
		else if (charChoice == 0)
			return;
		else
			break;
	} while (true);

	int useAmount = 1;

	if (temp->getStackSize() > 1)
	{
		cout << "How many would you like to use?" << endl;
		cout << "> ";
		do
		{
			if (cinCheck(useAmount, 0, temp->getStackSize()))
			{
				continue;
			}
			else if (useAmount == 0)
				return;
			else
				break;
		} while (true);

		cout << endl << "Item used. Healed " << temp->getConsumableEffect()* useAmount << " points." << endl;

		for (int i = 0; i < useAmount; i++)
		{
			_player->getCharacter(charChoice)->useConsumable(_player->getCharacter(charChoice), temp);
		}
	}
	else
	{
		cout << endl << "Item used. Healed " << temp->getConsumableEffect() << " points." << endl;
		_player->getCharacter(charChoice)->useConsumable(_player->getCharacter(charChoice), temp);
	}

	cout << _player->getCharacter(charChoice)->getName() << " (" <<
		_player->getCharacter(charChoice)->getCurrentHealth() << "/" <<
		_player->getCharacter(charChoice)->getMaxHealth() << ")" << endl;
}

bool WorldZone::equip(Equipment* temp)
{
	BaseItem* swap;
	int selection = -1;

	if (temp->getItemType() == BaseItem::WEAPON || temp->getItemType() == BaseItem::ARMOR)
	{
		do
		{
			cout << endl << "On which character?" << endl;
			_player->displayCharacters(temp->getSlot());
			cout << "> ";

			if (cinCheck(selection, 0, _player->getParty().size()))
			{
				_currentArea->showArea(_currentArea);
				cout << endl << endl;
			}
			else if (selection == 0) return false;
			else break;
		} while (true);

		swap = _player->getCharacter(selection)->equipItem(temp);
		if (swap != nullptr)
		{
			_player->pickUpItem(swap);
			cout << "Equipment swapped with what was equipped." << endl;
		}
		else
		{
			cout << "Item equipped." << endl;
		}

		return true;
	}
	else
	{
		cout << "You can't equip this item. " << temp->getItemType() << endl;
		Sleep(1000);
		return false;
	}
}

void WorldZone::stats()
{
	int choice = 0;

	do
	{
		cout << endl << "Which character will you check?" << endl;
		_player->displayCharacters();
		cout << "> ";

		if (cinCheck(choice, 0, _player->getParty().size()))
		{
			_currentArea->showArea(_currentArea);
			cout << endl << endl;
		}
		else if (choice == 0) return;
		else break;
	} while (true);

	do
	{
		system("cls");
		_player->getCharacter(choice)->displayStats();

		cout << endl << endl;

		_player->getCharacter(choice)->displayEquipment();

		cout << endl;

		char input;

		if (_player->hasItems())
			cout << "[E]quip ";

		if (_player->getCharacter(choice)->hasEquipment())
			cout << "[U]nequip ";

		cout << "[C]ancel" << endl << "> ";

		cin >> input;

		input = toupper(input);

		if (input == 'E' && _player->hasItems())
		{
			int slot = -1;
			int type = -1;

			do
			{
				cout << endl << "Pick a slot." << endl;
				cout << "> ";

				if (cinCheck(slot, Equipment::NOSLOT, Equipment::SLOTS::COUNT))
				{
					_player->getCharacter(choice)->displayStats();

					cout << endl << endl;

					_player->getCharacter(choice)->displayEquipment();
				}
				else if (slot == 0)
					return;
				else if (slot >= 1 && slot <= 5)
				{
					type = BaseItem::ITEM_TYPE::ARMOR;
					break;
				}
				else
				{
					type = BaseItem::ITEM_TYPE::WEAPON;
					break;
				}
			} while (true);


			vector<int> itemRange = _player->displayNums(type, slot - 1);
			int itemChoice;

			do
			{
				cout << endl << endl << "Which item would you like to use?" << endl;
				_player->displayInventory(type, itemRange[0], itemRange[1]);
				cout << "> ";

				if (cinCheck(itemChoice, 0, itemRange[1]))
				{
					_player->getCharacter(choice)->displayStats();

					cout << endl << endl;

					_player->getCharacter(choice)->displayEquipment();
				}
				else if (itemChoice == 0) return;
				else break;
			} while (true);

			itemChoice += itemRange[0];
			BaseItem* temp = _player->getInventoryItem(itemChoice);

			if (temp == nullptr)
			{
				cout << "Invalid option." << endl;
			}
			else
			{
				BaseItem* swap = _player->getCharacter(choice)->equipItem((Equipment*)temp);
				if (swap != nullptr)
				{
					_player->pickUpItem(swap);
					cout << "Equipment swapped with what was equipped." << endl;
				}
				else
				{
					cout << "Item equipped." << endl;
				}
			}

			_player->removeItem(itemChoice);

			Sleep(1000);
		}
		else if (input == 'U' && _player->getCharacter(choice)->hasEquipment())
		{
			unequip(_player->getCharacter(choice));
		}
		else if (input == 'C')
		{
			break;
		}
		else
		{

			cout << "Invalid choice." << endl;
			Sleep(1000);
		}
	} while (true);

}

void WorldZone::unequip(Character* character)
{
	int slot;

	do
	{
		cout << endl << "Which slot would you like to unequip?" << endl;
		cout << "[0] Cancel" << endl;
		cout << "> ";

		if (cinCheck(slot, Equipment::SLOTS::HEAD, Equipment::SLOTS::COUNT))
		{
			character->displayStats();

			cout << endl << endl;

			character->displayEquipment();
		}
		else if (slot == 0)	return;
		else break;
	} while (true);

	Equipment* temp = character->removeItem(slot - 1);

	if (temp != nullptr)
	{
		_player->pickUpItem(temp);
		cout << "Item removed" << endl;
	}
	else
	{
		cout << "Slot was empty" << endl;
	}

	Sleep(1000);

}