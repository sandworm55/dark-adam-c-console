#ifndef LIFE_FORM_H
#define LIFE_FORM_H

#include <iostream>
#include <string>
#include "Equipment.h"

using std::cout;
using std::endl;
using std::string;

class LifeForm
{
protected:
	string  _name;
	int _level;
	int _currentHealth;
	int _maxHealth;

	int _attack = 0;
	int _defence = 0;
	int _speed = 0;

	bool _isAlive;
	bool _isPlayerChar;

public:
	LifeForm(string name = "Rat", int level = 1, int maxHealth = 10,
		int attack = 10, int defence = 5, int speed = 5, bool isAlive = true, bool isPlayerChar = false) :
		_name(name), _level(level), _currentHealth(maxHealth), _maxHealth(maxHealth),
		_attack(attack), _defence(defence), _speed(speed), _isAlive(isAlive), _isPlayerChar(isPlayerChar)	{}

	virtual ~LifeForm() {}

	void setName(string name) { _name = name; }
	void setLevel(int level) { _level = level; }
	void adjustCurrentHealth(int health)
	{
		_currentHealth += health;
		_currentHealth = (getCurrentHealth() < 0 ? 0 : _currentHealth);
		_currentHealth = (getCurrentHealth() > getMaxHealth() ? _maxHealth : _currentHealth);
	}

	string getName() const { return _name; }
	int getLevel() const { return _level; }
	virtual int getCurrentHealth() const { return _currentHealth; }
	virtual int getMaxHealth() const { return _maxHealth; }
	virtual bool isAlive() const { return !(getCurrentHealth() <= 0); }
	bool isPlayerChar() const { return _isPlayerChar; }

	virtual void attack(LifeForm* LifeForm);

	virtual int receiveDamage(unsigned int damage);

	virtual int getStat(int stat);
	virtual int getAttack();
	virtual void displayStats();


private:

};


#endif