#ifndef ACTIVATION_EVENT_H
#define ACTIVATION_EVENT_H

#include "Event.h"
#include "Area.h"
#include "WorldZone.h"

class ActivationEvent : public Event
{
	Area* _firstArea;
	Area* _secondArea;

	Event* _eventToToggle;
	vector<Event*> _flags;
public:
	ActivationEvent(vector<Event*> flags, Event* eventToToggle, vector<BaseItem*>rewards, Event* evnt = nullptr) :
		Event("", rewards, true, Event::ACTIVE, evnt), _flags(flags),
		_firstArea(nullptr), _secondArea(nullptr), _eventToToggle(eventToToggle)
	{}

	ActivationEvent(vector<Event*> flags, Area* firstArea, Area* secondArea, vector<BaseItem*>rewards, Event* evnt = nullptr) :
		Event("", rewards, true, Event::ACTIVE, evnt), _flags(flags),
		_firstArea(firstArea), _secondArea(secondArea), _eventToToggle(nullptr)
	{}

	vector<BaseItem*> runEvent();
};

#endif