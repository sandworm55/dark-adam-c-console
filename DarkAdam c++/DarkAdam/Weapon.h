#ifndef WEAPON_H
#define WEAPON_H

#include "Equipment.h"

class Weapon : public Equipment
{
public:
	Weapon(string name, string description, int itemType,  int slot, int effect) :
		Equipment(name, description, ITEM_TYPE::WEAPON, SLOTS::WEAPON, effect)
	{}
};

#endif