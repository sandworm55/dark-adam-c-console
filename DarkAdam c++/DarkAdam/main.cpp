#include "WorldZone.h"
#include <ctime>

int main()
{
	srand(static_cast<unsigned int>(time(0)));
	WorldZone world;

	while (world.running());

	return 0;
}