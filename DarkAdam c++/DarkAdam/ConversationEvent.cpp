#include "ConversationEvent.h"

vector<BaseItem*> ConversationEvent::runEvent()
{
	system("cls");
	setActive(false);
	int count = 0;
	for each (string* var in _conversation)
	{
		for each (char chr in *var)
		{
			int time = 0;
			cout << chr;
			if (chr == '.')
				Sleep(time * 20);
			else
				Sleep(time);
		}
		cout << std::endl<<std::endl;
		if (count < (int)_conversation.size())
			system("PAUSE > nul");
		count += 1;
	}
	runCallback();
	return getRewards();
}