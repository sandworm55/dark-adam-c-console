#ifndef __ROOM_H
#define __ROOM_H

#include <string>
#include <vector>
#include "BaseItem.h"
#include "Event.h"

using std::string;
using std::vector;

class Room
{
public:
	friend class Area;

	Room(string name, string description = "A dark void.", bool areaExit = false) :
		_roomName(name), _description(description), _areaExit(areaExit) {}
	~Room() {}

	//output info
	string getName() { return _roomName; }
	string getDesc() { return _description; }

	//exits
	Room* getRoomExit(int index) const { return _exits[index]; }
	int getExitSize() { return (int)_exits.size(); }
	

	void listExits();
	bool setRoomExit(int index, Room* room);
	void removeRoomExit(int index);

	//access to area
	bool hasAreaExit() { return _areaExit; }

	//items
	void addItem(BaseItem* item);
	BaseItem* removeItem(size_t index);
	void listItems();
	bool hasItems();

	//events
	void addEvent(Event* evnt) { _events.push_back(evnt); }
	void addActiveEvent(Event* evnt) { _activeEvents.push_back(evnt); }
	void addCombatEvent(Event* evnt) { _combatEvents.push_back(evnt); }
	bool hasEvent();
	void listEvents();
	int getEventSize() { return (int)_events.size(); }

	vector<Event*> getCombatEvent() { return _combatEvents; }
	vector<BaseItem*> runEvent(size_t index);
	vector<Event*> getEvents() { return _events; }
	vector<Event*> getCombatEvents() { return _combatEvents; }
private:
	string _roomName;
	string _description;
	bool _areaExit;
	bool _visited;

	vector<Room*> _exits;
	vector<BaseItem*> _items;
	vector<Event*> _events;
	vector<Event*> _activeEvents;
	vector<Event*> _combatEvents;
};

#endif